# README #

R6Stats est une application permettant de pouvoir visualiser les statistiques d'un joueur donné.

Utilisation de l'API : https://dev.r6stats.com/

Documentation : https://dev.r6stats.com/docs/

Base url : https://api.r6stats.com/api/v1

### Summary ###

* User stories
* Libraries
* Models

### User stories ###

* Ajout
    * Pierre sélectionne l'item « PS4 » dans la liste des consoles, il clique sur l'icône « + » et entre le pseudo d'un joueur. Le joueur est reconnu et ajouté à la liste des joueurs PS4.
    * Pierre sélectionne l'item « PS4 » dans la liste des consoles, il clique sur l'icône « + » et entre le pseudo d'un joueur. Le joueur n'est pas reconnu. Le message suivant apparait : « Nous n'avons trouvé aucun joueur du nom de PSEUDO sur la plateforme sélectionnée ».
    * Pierre sélectionne l'item « PS4 » dans la liste des consoles, il clique sur l'icône « + » et entre le pseudo d'un joueur. Aucune connexion internet n'est détecté. Le message suivant apparait « Vous devez être connecté à internet pour pouvoir ajouter un joueur. ».
* Consultation
    * Pierre sélectionne l'item « PS4 » dans la liste des consoles, il clique sur « KoWcOw_59 ». Une page avec les statistiques de ce joueur s'ouvre.
    * Pierre sélectionne l'item « PS4 » dans la liste des consoles, il clique sur « KoWcOw_59 ». Une page avec les statistiques de ce joueur s'ouvre. Aucune connexion internet n'est détecté. Le message suivant apparait « Vous devez être connecté à internet pour pouvoir actualiser les statistiques de ce joueur. ».
* Suppression
    * Pierre sélection l’item « PS4 » dans la liste des consoles, il glisse l’item « KoWcOw_59 » vers la gauche (ou la droite).
    * Pierre sélection l’item « PS4 » dans la liste des consoles, il glisse l’item « KoWcOw_59 » vers la gauche (ou la droite). Il clique sur « Annuler » pour annuler la suppression.

### Libraries ###

- [RxAndroid](https://github.com/ReactiveX/RxAndroid)
- [Anko](https://github.com/Kotlin/anko)
- [EventBus](https://github.com/greenrobot/EventBus)
- [Glide](https://github.com/bumptech/glide)
- [RetroFit](http://square.github.io/retrofit/)
- [Fabric](https://get.fabric.io/)

### Models ###
* Maquette d'écran bientôt disponible