package stamper.com.r6stats.app.widgets.views

import android.app.AlertDialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import stamper.com.r6stats.R

/**
 * @author Corentin Stamper
 * @date 13/11/2017.
 */
class R6ProgressBar(context: Context): AlertDialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_progress)

        window.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
    }

}