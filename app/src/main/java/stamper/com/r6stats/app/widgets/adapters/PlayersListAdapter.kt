package stamper.com.r6stats.app.widgets.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.EventLog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_player.view.*
import org.greenrobot.eventbus.EventBus
import stamper.com.r6stats.R
import stamper.com.r6stats.app.controllers.events.ClickPlayerEvent
import stamper.com.r6stats.app.controllers.events.LongClickPlayerEvent
import stamper.com.r6stats.core.models.Player

/**
 * @author Corentin Stamper
 * @date 09/11/2017.
 */
class PlayersListAdapter(val context: Context, val items: List<Player>): RecyclerView.Adapter<PlayersListAdapter.PlayerVH>() {

    override fun onBindViewHolder(holder: PlayerVH?, position: Int) {
        val player = items.get(position)
        holder!!.itemView.username.text = player.username
        holder.itemView.setOnClickListener {
            EventBus.getDefault().post(ClickPlayerEvent(player.ubisoftId))
        }

        holder.itemView.setOnLongClickListener {
            EventBus.getDefault().post(LongClickPlayerEvent(player))
            true
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): PlayerVH {
        val view = LayoutInflater.from(context).inflate(R.layout.item_player, parent, false)
        return PlayerVH(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class PlayerVH(view: View): RecyclerView.ViewHolder(view)
}