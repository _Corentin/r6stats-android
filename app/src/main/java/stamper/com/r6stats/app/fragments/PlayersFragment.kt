package stamper.com.r6stats.app.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_players.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import stamper.com.r6stats.R
import stamper.com.r6stats.app.controllers.events.PlayerAddedEvent
import stamper.com.r6stats.app.controllers.events.PlayerRemovedEvent
import stamper.com.r6stats.app.widgets.adapters.PlayersListAdapter
import stamper.com.r6stats.core.databases.DatabaseManager
import stamper.com.r6stats.core.models.Platform
import stamper.com.r6stats.core.models.Player

/**
 * @author Corentin Stamper
 * @date 02/11/2017.
 */
class PlayersFragment: Fragment() {

    private var platform: Platform = Platform.ALL
    private var players: List<Player>? = null

    companion object {
        fun create(platform: Platform): PlayersFragment {
            val fragment = PlayersFragment()
            fragment.platform = platform
            fragment.players = ArrayList()
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_players, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initPlayersList()
    }

    fun initPlayersList() {
        players = DatabaseManager.INSTANCE.listPlayers(platform.value)

        if (players?.size == 0) {
            recyclerView.visibility = View.GONE
            noPlayers.visibility = View.VISIBLE

            noPlayers.text = String.format(getString(R.string.players_empty_list), platform.title)
        } else {
            recyclerView.visibility = View.VISIBLE
            noPlayers.visibility = View.GONE

            recyclerView.adapter = PlayersListAdapter(context, players!!)
            recyclerView.layoutManager = LinearLayoutManager(context)
        }
    }

    override fun onResume() {
        super.onResume()
        EventBus.getDefault().register(this)
    }

    override fun onPause() {
        super.onPause()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe fun onEvent(event: PlayerAddedEvent) {
        initPlayersList()
    }

    @Subscribe fun onEvent(event: PlayerRemovedEvent) {
        initPlayersList()
    }
}