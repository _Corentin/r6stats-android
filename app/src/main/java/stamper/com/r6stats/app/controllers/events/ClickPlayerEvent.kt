package stamper.com.r6stats.app.controllers.events

/**
 * @author Corentin Stamper
 * @date 13/11/2017.
 */
class ClickPlayerEvent(val playerId: String)