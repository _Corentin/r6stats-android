package stamper.com.r6stats.app.controllers.events

/**
 * @author Corentin Stamper
 * @date 09/11/2017.
 */
class AddPlayerEvent(val username: String, val platform: String)