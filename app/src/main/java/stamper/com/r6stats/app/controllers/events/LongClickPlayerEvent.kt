package stamper.com.r6stats.app.controllers.events

import stamper.com.r6stats.core.models.Player

/**
 * @author Corentin Stamper
 * @date 13/11/2017.
 */
class LongClickPlayerEvent(val player: Player)