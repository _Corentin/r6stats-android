package stamper.com.r6stats.app.activities

import android.content.Context
import android.content.Intent
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import io.reactivex.android.schedulers.AndroidSchedulers
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import stamper.com.r6stats.R
import stamper.com.r6stats.app.controllers.events.*
import stamper.com.r6stats.app.widgets.views.R6ProgressBar
import stamper.com.r6stats.core.databases.DatabaseHelper
import stamper.com.r6stats.core.databases.DatabaseManager
import stamper.com.r6stats.core.models.constants.Tags
import stamper.com.r6stats.core.services.PlayerManager

/**
 * @author Corentin Stamper
 * @date 03/11/2017.
 */
open class BaseActivity: AppCompatActivity() {

    var loader: R6ProgressBar? = null

    companion object {
        val EXTRA_PLAYER_ID = "EXTRA_PLAYER_ID"
    }

    fun showLoader() {
        runOnUiThread {
            if (loader != null) {
                loader?.dismiss()
                loader = null
            }

            loader = R6ProgressBar(this)
            loader?.setCancelable(false)
            loader?.show()
        }
    }

    fun hideLoader() {
        runOnUiThread {
            if (loader != null) {
                loader?.dismiss()
                loader = null
            }
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe fun onEvent(event: ErrorEvent) {
        Log.e(Tags.WS, "Error 3")
        hideLoader()
        Log.e(Tags.WS, "Error 4")
        AlertDialog.Builder(this)
                .setTitle(event.error.title)
                .setMessage(event.error.meta.description)
                .setNeutralButton(R.string.dialog_error_neutral_button, null)
                .show()
        Log.e(Tags.WS, "Error 5")
    }

    @Subscribe fun onEvent(event: AddPlayerEvent) {
        showLoader()
        PlayerManager.INSTANCE.getPlayer(event.username, event.platform).observeOn(AndroidSchedulers.mainThread())
                .subscribe({ playerResponse ->
                    val player = playerResponse.player
                    val result = DatabaseManager.INSTANCE.addPlayer(player)
                    if (result) {
                        EventBus.getDefault().post(PlayerAddedEvent())
                        Toast.makeText(applicationContext, playerResponse.player.username + " added", Toast.LENGTH_SHORT).show()
                    } else {
                        // Show error in dialog
                        Toast.makeText(applicationContext, playerResponse.player.username + " not added", Toast.LENGTH_SHORT).show()
                    }
                    hideLoader()
                })
    }

    @Subscribe fun onEvent(event: ClickPlayerEvent) {
        val intent = Intent(this, PlayerDetailsActivity::class.java)
        intent.putExtra(BaseActivity.EXTRA_PLAYER_ID, event.playerId)
        startActivity(intent)
    }

    @Subscribe fun onEvent(event: LongClickPlayerEvent) {
        val message = String.format(getString(R.string.dialog_remove_player_description), event.player.username)

        android.app.AlertDialog.Builder(this)
                .setTitle(R.string.dialog_remove_player_title)
                .setMessage(message)
                .setPositiveButton(R.string.dialog_remove_player_positive_button) {
                    dialogInterface, i ->
                    if (DatabaseManager.INSTANCE.removePlayer(event.player))
                        EventBus.getDefault().post(PlayerRemovedEvent())
                }
                .setNeutralButton(R.string.dialog_remove_player_neutral_button, null)
                .show()
    }
}
