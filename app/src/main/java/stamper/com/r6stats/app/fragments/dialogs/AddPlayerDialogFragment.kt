package stamper.com.r6stats.app.fragments.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import kotlinx.android.synthetic.main.dialog_add_player.view.*
import org.greenrobot.eventbus.EventBus
import stamper.com.r6stats.R
import stamper.com.r6stats.app.controllers.events.AddPlayerEvent
import stamper.com.r6stats.core.models.Platform

/**
 * @author Corentin Stamper
 * @date 03/11/2017.
 */
class AddPlayerDialogFragment: DialogFragment() {

    private var platform: Platform = Platform.ALL

    companion object {
        val tag = "R6_ADD_PLAYER_DIALOG_FRAGMENT"

        fun create(platform: Platform): AddPlayerDialogFragment {
            val fragment = AddPlayerDialogFragment()
            fragment.platform = platform
            return fragment
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val title = String.format(getString(R.string.dialog_add_player_title), platform.title).toUpperCase()
        val view = activity.layoutInflater.inflate(R.layout.dialog_add_player, null)
        val description = getString(R.string.dialog_add_player_description)

        view.description.text = description

        return AlertDialog.Builder(activity)
                .setTitle(title)
                .setView(view)
                .setPositiveButton(R.string.dialog_add_player_positive_button) {
                    dialogInterface, i ->
                    val username = view.username.text.toString()
                    // Todo : detect empty string
                    EventBus.getDefault().post(AddPlayerEvent(username, platform.value))
                }
                .setNeutralButton(R.string.dialog_add_player_neutral_button, null)
                .create()
    }
}