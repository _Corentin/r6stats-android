package stamper.com.r6stats.app.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import stamper.com.r6stats.R

/**
 * @author Corentin Stamper
 * @date 02/11/2017.
 */
class EmptyFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_empty, container, false)
    }

}
