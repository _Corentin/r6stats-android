package stamper.com.r6stats.app.activities

import android.os.Bundle
import stamper.com.r6stats.core.services.PlayerManager

/**
 * @author Corentin Stamper
 * @company Phoceis
 * @email cstamper@phoceis.com
 * @date 08/12/2017.
 */
class PlayerDetailsActivity: BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (intent == null)
            finish()

        // Player ID
        val playerId = intent.getStringExtra(BaseActivity.EXTRA_PLAYER_ID)
        if (playerId == null)
            finish()

        // Player Stats

    }
}