package stamper.com.r6stats.app.widgets.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import stamper.com.r6stats.app.fragments.EmptyFragment
import stamper.com.r6stats.app.fragments.PlayersFragment
import stamper.com.r6stats.core.databases.DatabaseManager
import stamper.com.r6stats.core.models.Platform

/**
 * @author Corentin Stamper
 * @date 02/11/2017.
 */
class PlayersViewPagerAdapter(fm: FragmentManager): FragmentStatePagerAdapter(fm) {

    var platforms: Array<Platform>

    init {
        platforms = arrayOf(Platform.ALL, Platform.PS4, Platform.XONE, Platform.UPLAY)
    }

    override fun getPageTitle(position: Int): CharSequence {
        return platforms.get(position).title
    }

    override fun getItem(position: Int): Fragment {
        if (position > platforms.size)
            return EmptyFragment()
        return PlayersFragment.create(platforms.get(position))
    }

    override fun getCount(): Int {
        return platforms.size
    }

    fun getTypeWith(position: Int) : Platform {
        if (position > platforms.size)
            return Platform.ALL
        return platforms.get(position)
    }
}
