package stamper.com.r6stats.app.controllers.events

/**
 * @author Corentin Stamper
 * @company Phoceis
 * @email cstamper@phoceis.com
 * @date 08/12/2017.
 */
class PlayerAddedEvent