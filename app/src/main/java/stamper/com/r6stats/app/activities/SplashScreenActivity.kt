package stamper.com.r6stats.app.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import kotlinx.android.synthetic.main.activity_splashscreen.*
import stamper.com.r6stats.R

/**
 * @author Corentin Stamper
 * @date 13/11/2017.
 */
class SplashScreenActivity : BaseActivity() {

    val delay = 1000L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashscreen)

        val handler = Handler()
        val runnable = Runnable {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        handler.postDelayed(runnable, delay)
    }

}