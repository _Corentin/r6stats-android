package stamper.com.r6stats.app.controllers.events

import stamper.com.r6stats.core.models.Error

/**
 * @author Corentin Stamper
 * @company Phoceis
 * @email cstamper@phoceis.com
 * @date 27/12/2017.
 */
data class ErrorEvent(val error: Error)