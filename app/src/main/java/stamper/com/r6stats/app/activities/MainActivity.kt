package stamper.com.r6stats.app.activities

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import stamper.com.r6stats.R
import stamper.com.r6stats.app.fragments.dialogs.AddPlayerDialogFragment
import stamper.com.r6stats.app.widgets.adapters.PlayersViewPagerAdapter
import stamper.com.r6stats.core.models.Platform

/**
 * @author Corentin Stamper
 * @date 02/11/2017.
 */
class MainActivity: BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Viewpager to show registered players [All|PS4|XONE|PC]
        viewPager.adapter = PlayersViewPagerAdapter(supportFragmentManager)
        tabLayout.setupWithViewPager(viewPager)
        tabLayout.tabMode = TabLayout.MODE_FIXED

        // Hide add button in All Players section
        addButton.visibility = View.GONE
        viewPager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                val type = (viewPager.adapter as PlayersViewPagerAdapter).getTypeWith(position)
                if (type.equals(Platform.ALL))
                    addButton.visibility = View.GONE
                else
                    addButton.visibility = View.VISIBLE
            }
        })

        // Add button
        addButton.setOnClickListener { v ->
            val type = (viewPager.adapter as PlayersViewPagerAdapter).getTypeWith(viewPager.currentItem)
            val dialog = AddPlayerDialogFragment.create(type)
            dialog.show(supportFragmentManager, AddPlayerDialogFragment.tag)
        }
    }
}