package stamper.com.r6stats.core.databases

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*
import stamper.com.r6stats.core.models.Player

/**
 * @author Corentin Stamper
 * @date 03/11/2017.
 */
class DatabaseHelper(context: Context): ManagedSQLiteOpenHelper(context, "Database", null, 1) {

    companion object {
        private var instance: DatabaseHelper? = null

        @Synchronized fun Instance(context: Context): DatabaseHelper {
            if (instance == null) instance = DatabaseHelper(context)
            return instance!!
        }
    }

    override fun onCreate(database: SQLiteDatabase?) {
        database?.createTable(
                Player.TABLE_NAME,
                true,
                Player.COLUMN_UBISOFT_ID to TEXT + PRIMARY_KEY,
                Player.COLUMN_USERNAME to TEXT,
                Player.COLUMN_PLATFORM to TEXT)
    }

    override fun onUpgrade(database: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        database?.dropTable(Player.TABLE_NAME, true)
    }
}