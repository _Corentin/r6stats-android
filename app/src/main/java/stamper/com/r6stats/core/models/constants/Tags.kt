package stamper.com.r6stats.core.models.constants

/**
 * @author Corentin Stamper
 * @company Phoceis
 * @email cstamper@phoceis.com
 * @date 27/12/2017.
 */
object Tags {
    val WS = "R6_WebServices"
    val DB = "R6_DataBases"
    val VW = "R6_Views"
}