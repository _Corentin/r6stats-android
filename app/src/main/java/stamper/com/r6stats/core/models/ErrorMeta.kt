package stamper.com.r6stats.core.models

import com.google.gson.annotations.SerializedName

/**
 * @author Corentin Stamper
 * @company Phoceis
 * @email cstamper@phoceis.com
 * @date 27/12/2017.
 */
data class ErrorMeta(
        @SerializedName("description") val description: String
)