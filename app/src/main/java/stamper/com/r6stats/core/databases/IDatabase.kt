package stamper.com.r6stats.core.databases

import stamper.com.r6stats.core.models.Player

/**
 * @author Corentin Stamper
 * @date 09/11/2017.
 */
interface IDatabase {

    /**
     * Add player to database
     * @param player Player to add
     * @return true if player has added, false otherwise
     */
    fun addPlayer(player: Player): Boolean

    /**
     * List all player added into database filtered by platform
     * @param platform Platform filter
     * @return a list of player
     */
    fun listPlayers(platform: String): ArrayList<Player>

    /**
     * Remove player from database
     * @param player Player to remove
     * @return true if player has removed, false otherwise
     */
    fun removePlayer(player: Player): Boolean

}