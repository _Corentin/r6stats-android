package stamper.com.r6stats.core.models

import com.google.gson.annotations.SerializedName

/**
 * @author Corentin Stamper
 * @date 02/11/2017.
 */
data class PlayerStats (
        @SerializedName("ranked")      val ranked: PlayerStatsGamemode,
        @SerializedName("casual")      val casual: PlayerStatsGamemode,
        @SerializedName("overall")     val overall: PlayerStatsOverall,
        @SerializedName("progression") val progression: PlayerStatsProgression
)