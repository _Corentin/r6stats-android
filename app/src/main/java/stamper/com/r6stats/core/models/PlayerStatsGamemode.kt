package stamper.com.r6stats.core.models

import com.google.gson.annotations.SerializedName

/**
 * @author Corentin Stamper
 * @date 02/11/2017.
 */
data class PlayerStatsGamemode (
        @SerializedName("has_played") val hasPlayed: Boolean,
        @SerializedName("wins") val wins: Int,
        @SerializedName("losses") val loses: Int,
        @SerializedName("kills") val kills: Int,
        @SerializedName("deaths") val deaths: Int,
        @SerializedName("playtime") val playtime: Int
)