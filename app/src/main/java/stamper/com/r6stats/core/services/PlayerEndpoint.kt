package stamper.com.r6stats.core.services

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import stamper.com.r6stats.core.models.PlayerResponse

/**
 * @author Corentin Stamper
 * @date 02/11/2017.
 */
interface PlayerEndpoint {

    @GET("players/{username}")
    fun player(@Path("username") username: String, @Query("platform") platform: String) : Observable<PlayerResponse>

}