package stamper.com.r6stats.core.models

/**
 * @author Corentin Stamper
 * @date 02/11/2017.
 */
data class PlayerResponse (val player: Player)
