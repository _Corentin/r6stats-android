package stamper.com.r6stats.core.databases

import android.content.Context
import android.util.Log
import org.jetbrains.anko.db.MapRowParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import stamper.com.r6stats.core.models.Platform
import stamper.com.r6stats.core.models.Player

/**
 * @author Corentin Stamper
 * @date 09/11/2017.
 */
enum class DatabaseManager: IDatabase {

    INSTANCE;

    private lateinit var helper: DatabaseHelper

    fun init(context: Context) {
        helper = DatabaseHelper(context)
    }

    override fun addPlayer(player: Player): Boolean = helper.use {
        val id = insert(Player.TABLE_NAME, Player.COLUMN_UBISOFT_ID to player.ubisoftId,
                Player.COLUMN_USERNAME to player.username, Player.COLUMN_PLATFORM to player.platform)
        id != -1L
    }

    override fun listPlayers(platform: String): ArrayList<Player> {
        var players = ArrayList<Player>()

        helper.use {
            if (platform != Platform.ALL.value) {
                select(Player.TABLE_NAME).whereArgs("(" + Player.COLUMN_PLATFORM + " = {platform})", "platform" to platform).parseList(object : MapRowParser<List<Player>> {
                    override fun parseRow(columns: Map<String, Any?>): List<Player> {
                        val ubisoftId = columns.getValue(Player.COLUMN_UBISOFT_ID) as String
                        val username = columns.getValue(Player.COLUMN_USERNAME) as String
                        val platform = columns.getValue(Player.COLUMN_PLATFORM) as String

                        players.add(Player(username = username, platform = platform, ubisoftId = ubisoftId))
                        return players
                    }
                })
            } else {
                select(Player.TABLE_NAME).parseList(object : MapRowParser<List<Player>> {
                    override fun parseRow(columns: Map<String, Any?>): List<Player> {
                        val ubisoftId = columns.getValue(Player.COLUMN_UBISOFT_ID) as String
                        val username = columns.getValue(Player.COLUMN_USERNAME) as String
                        val platform = columns.getValue(Player.COLUMN_PLATFORM) as String

                        players.add(Player(username = username, platform = platform, ubisoftId = ubisoftId))
                        return players
                    }
                })
            }
        }

        return players
    }

    override fun removePlayer(player: Player): Boolean = helper.use {
        val id = delete(Player.TABLE_NAME, Player.COLUMN_UBISOFT_ID + " = {id}", "id" to player.ubisoftId)
        id > 0
    }
}