package stamper.com.r6stats.core.models

/**
 * @author Corentin Stamper
 * @date 02/11/2017.
 */
enum class Platform(val title: String, val value: String) {
    PS4("PS4", "ps4"),
    UPLAY("PC", "uplay"),
    XONE("XONE", "xone"),
    ALL("ALL", "")
}