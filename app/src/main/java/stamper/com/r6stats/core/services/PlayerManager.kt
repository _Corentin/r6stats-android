package stamper.com.r6stats.core.services

import android.content.Context
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import stamper.com.r6stats.core.RetrofitBase
import stamper.com.r6stats.core.models.Player
import stamper.com.r6stats.core.models.PlayerResponse

/**
 * @author Corentin Stamper
 * @date 02/11/2017.
 */
enum class PlayerManager {

    INSTANCE;

    private lateinit var endpoint: PlayerEndpoint

    fun init(context: Context) {
        endpoint = RetrofitBase(context).create(PlayerEndpoint::class.java)
    }

    fun getPlayer(username: String, platform: String) : Observable<PlayerResponse> {
        return endpoint.player(username, platform)
                .subscribeOn(Schedulers.io())
    }
}
