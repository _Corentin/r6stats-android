package stamper.com.r6stats.core.models

import com.google.gson.annotations.SerializedName

/**
 * @author Corentin Stamper
 * @date 02/11/2017.
 */
data class Player(
        @SerializedName("username")   val username: String,
        @SerializedName("platform")   val platform: String,
        @SerializedName("ubisoft_id") val ubisoftId: String,
        @SerializedName("stats")      val stats: PlayerStats? = null
) {
    companion object {
        val TABLE_NAME = "players"
        val COLUMN_USERNAME = "username"
        val COLUMN_PLATFORM = "platform"
        val COLUMN_UBISOFT_ID = "ubisoft_id"
    }

    override fun toString(): String {
        return ubisoftId + ": [" + platform + "]" + username
    }
}