package stamper.com.r6stats.core.models.constants

/**
 * @author Corentin Stamper
 * @company Phoceis
 * @email cstamper@phoceis.com
 * @date 27/12/2017.
 */
object Errors {
    val CODE_404 = 404
    val CODE_500 = 500

    val STATUS_FAILED = "failed"
}