package stamper.com.r6stats.core.models

import com.google.gson.annotations.SerializedName

/**
 * @author Corentin Stamper
 * @company Phoceis
 * @email cstamper@phoceis.com
 * @date 27/12/2017.
 */
data class ErrorResponse(
        @SerializedName("status") val status: String,
        @SerializedName("errors") val errors: List<Error>
) {
    override fun toString(): String {
        var toString = "status: " + status + "\n"
        for (error in errors) {
            toString += "**************\n"
            toString += "*" + error + "\n"
            toString += "**************\n"
        }
        return toString
    }
}