package stamper.com.r6stats.core

import android.content.Context
import android.util.Log
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.runOnUiThread
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import stamper.com.r6stats.core.models.ErrorResponse
import stamper.com.r6stats.core.models.constants.Errors
import stamper.com.r6stats.core.models.constants.Tags
import java.util.concurrent.TimeUnit

/**
 * @author Corentin Stamper
 * @date 02/11/2017.
 */
class RetrofitBase(val context: Context) {
    private val retrofit: Retrofit

    init {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        val errorInterceptor = Interceptor { chain ->
            val request = chain.request()
            val response = chain.proceed(request)

            if (!response.isSuccessful) {
                val error = parse(response, retrofit())
                if (error.status == Errors.STATUS_FAILED) {
                    Log.e(Tags.WS, "Error 1")
                    context.runOnUiThread {
                        EventBus.getDefault().post(error.errors.get(0))
                    }
                    Log.e(Tags.WS, "Error 2")
                }
            }

            response
        }

        val builder = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .addInterceptor(errorInterceptor)
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)

        retrofit = Retrofit.Builder()
                .baseUrl("https://api.r6stats.com/api/v1/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build())
                .build()
    }

    fun retrofit(): Retrofit {
        return retrofit
    }

    fun <T> create(service: Class<T>): T {
        return retrofit().create(service)
    }

    private fun parse(response: Response, retrofit: Retrofit): ErrorResponse {
        val converter: Converter<ResponseBody, ErrorResponse> = retrofit.responseBodyConverter(ErrorResponse::class.java, emptyArray<Annotation>())
        return converter.convert(response.body()!!)
    }
}