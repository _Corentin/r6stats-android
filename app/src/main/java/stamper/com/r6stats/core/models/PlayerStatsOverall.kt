package stamper.com.r6stats.core.models

import com.google.gson.annotations.SerializedName

/**
 * @author Corentin Stamper
 * @date 02/11/2017.
 */
data class PlayerStatsOverall (
        @SerializedName("revives")                 val revives: Int,
        @SerializedName("suicides")                val suicides: Int,
        @SerializedName("reinforcements_deployed") val reinforcementsDeployed: Int,
        @SerializedName("barricades_built")        val barricadesBuild: Int,
        @SerializedName("steps_moved")             val stepsMoved: Int,
        @SerializedName("bullets_fired")           val bulletsFired: Int,
        @SerializedName("bullets_hit")             val bulletsHit: Int,
        @SerializedName("headshots")               val headshots: Int,
        @SerializedName("melee_kills")             val meleeKills: Int,
        @SerializedName("penetration_kills")       val penetrationKills: Int,
        @SerializedName("assists")                 val assists: Int
)