package stamper.com.r6stats.core.models

import com.google.gson.annotations.SerializedName

/**
 * @author Corentin Stamper
 * @date 02/11/2017.
 */
data class PlayerStatsProgression (
        @SerializedName("level") val level: Int,
        @SerializedName("xp")    val xp: Int
)