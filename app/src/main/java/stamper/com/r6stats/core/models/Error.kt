package stamper.com.r6stats.core.models

import com.google.gson.annotations.SerializedName

/**
 * @author Corentin Stamper
 * @company Phoceis
 * @email cstamper@phoceis.com
 * @date 27/12/2017.
 */
data class Error(
        @SerializedName("detail") val detail: String,
        @SerializedName("title") val title: String,
        @SerializedName("code") val code: String,
        @SerializedName("meta") val meta: ErrorMeta,
        @SerializedName("reference") val reference: String,
        @SerializedName("field") val field: String
) {
    override fun toString(): String {
        return  "detail: " + detail + "\n" +
                "title: " + title + "\n" +
                "code: " + code + "\n" +
                "meta: " + meta.description + "\n" +
                "reference: " + reference + "\n" +
                "field: " + field
    }
}