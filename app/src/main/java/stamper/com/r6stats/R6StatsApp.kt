package stamper.com.r6stats

import android.app.Application
import stamper.com.r6stats.core.databases.DatabaseManager
import stamper.com.r6stats.core.services.PlayerManager

/**
 * @author Corentin Stamper
 * @date 02/11/2017.
 */
class R6StatsApp: Application() {

    override fun onCreate() {
        super.onCreate()
        PlayerManager.INSTANCE.init(applicationContext)
        DatabaseManager.INSTANCE.init(applicationContext)
    }
}
